
Redd'n'Tank Trenches-Standalone Version 1.0.1
24.04.2020

The Team:
Redd: Config, functions, model, textures
Tank: Model, textures

Redd'n'Tank official website:
http://reddiem.portfoliobox.net/

Discuss AW-Forum (german): 
https://armaworld.de/index.php?thread/3258-redd-vehicles-marder-1a5-release/

Discuss BI-Forum (english):
https://forums.bistudio.com/forums/topic/210296-redd-vehicles-marder-1a5-release/

Steam Workshop:
https://steamcommunity.com/sharedfiles/filedetails/?id=1983487115

YouTube:
https://www.youtube.com/playlist?list=PLJVKqjrLClHTeQcHtncGIJdRyHiTblsdT

Imgur:
https://imgur.com/a/Uv1Ei

GitLab: (See the whole Changelog)
https://gitlab.com/TTTRedd/rnt_trenches_standalone
pls report issues here

Contact:
redd.arma.modding@gmail.com

Addon Contains:
- Trench Modules - Consists of modular parts for building trenches, can be found under Empty->Structures->RnT Structures

Features:
Trench Modules consists of 
- Bunker module
- Corner module
- End module
- Straight module
- Stand module
- Position module
- 'T' module

Changelog:
Version 1.0.1
- Changed resized the stand for the trenches (it's larger now)
Version 1.0.0
- Release Trench Modules as standalone version

License:
Licensed Under Attribution-NonCommercial-NoDerivs CC BY-NC-ND
https://creativecommons.org/licenses/by-nc-nd/4.0/

Thanks:
- Thanks to T4nk for joining me
- Thanks to Tactical Training Team for testing and support
- Last but not least, thanks to our wifes, Redds daughter and Tanks cat for giving us the time to do all this stuff :*